import zmq
import time
import random
import logging
import itertools
from struct import pack
from manager import ManagedProcess, ProcessRole
from math import sin

logger = logging.getLogger(__name__)
logger.level = logging.INFO


class Publisher(object):
    def __init__(self, host: str, port: int):
        # Prepare our context and publisher
        self.context = zmq.Context()
        self.publisher = self.context.socket(zmq.PUB)
        self.publisher.bind(f"tcp://{host}:{port}")

    def send(self, topic: str, message):
        self.publisher.send_multipart([topic.encode(), pack('f', message)])


class Fizzer(ManagedProcess):
    ROLE = ProcessRole.PRODUCER

    def __init__(self, host: str, port: int, name: str,
                 address: tuple, authkey: bytes, role: ProcessRole,
                 topic: str):
        super().__init__(host=host, port=port, name=name, address=address, authkey=authkey, role=role)

        self.topic = topic
        self.publisher = Publisher(host, port)
        logging.warning(self.manager.lookup_process(self.name))


    def run(self):
        while True:
            for i in itertools.cycle(range(0,3141,1)):
                self.publisher.send('test', sin(i/10))
                time.sleep(0.02)


if __name__ == '__main__':
    m = Fizzer(host='127.0.0.1', port=8888, name='test',
               address=('127.0.0.1', 50000), authkey=b'abracadabra',
               role=ProcessRole.PRODUCER, topic='test')

    m.run()
