import zmq
import queue
import logging
from struct import unpack
from manager import ManagedProcess, ProcessRole

#from multiprocessing import shared_memory


logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')

logger = logging.getLogger(name=__name__)
logger.level = logging.INFO

class Consumer(object):
    def __init__(self, host: str, port: int, subscription: str):

        # Prepare our context and publisher
        self.context = zmq.Context()
        self.subscriber = self.context.socket(zmq.SUB)
        self.subscriber.connect(f"tcp://{host}:{port}")
        self.subscriber.setsockopt(zmq.SUBSCRIBE, subscription)


    def __del__(self):
        self.subscriber.close()
        self.context.term()


class MemoryManager(ManagedProcess):
    ROLE = ProcessRole.CONSUMER

    def __init__(self, name: str, address: tuple, authkey: bytes, topic: str):
        super().__init__(
                host=None, port=None, name=name,
                role=ProcessRole.CONSUMER, address=address,
                authkey=authkey
                )

        #self.queue = queue.Queue()

        # a = np.array([1, 1, 2, 3, 5, 8])  # Start with an existing NumPy array
        # self.shm = shared_memory.SharedMemory(create=True, size=a.nbytes, name='mymem')
        # # Now create a NumPy array backed by shared memory
        # b = np.ndarray(a.shape, dtype=a.dtype, buffer=self.shm.buf)
        # b[:] = a[:]  # Copy the original data into shared memory

        data = self.manager.lookup_process('test')
        print(data)
        self.consumer = Consumer(data[2], data[3], topic.encode())

    def run(self):
        while True:
            # Read envelope with address

            [address, contents] = self.consumer.subscriber.recv_multipart()
            print("[%s] %s" % (address, unpack('f', contents)))
            #self.queue.put(contents)


if __name__ == '__main__':
    m = MemoryManager(name='test_consumer', address=('127.0.0.1', 50000),
                      authkey=b'abracadabra', topic='test')

    m.run()
