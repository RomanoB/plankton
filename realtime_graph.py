from bokeh.driving import count
from bokeh.models import ColumnDataSource
from bokeh.plotting import curdoc, figure

import zmq
from manager import ManagedProcess, ProcessRole
from struct import unpack
import logging

logger = logging.getLogger(__name__)
logger.level = logging.INFO

class Consumer(object):
    def __init__(self, host: str, port: int, subscription: bytes):

        # Prepare our context and publisher
        self.context = zmq.Context()
        self.subscriber = self.context.socket(zmq.SUB)
        self.subscriber.connect(f"tcp://{host}:{port}")
        self.subscriber.setsockopt(zmq.SUBSCRIBE, subscription)

    def __del__(self):
        self.subscriber.close()
        self.context.term()


class MemoryManager(ManagedProcess):
    ROLE = ProcessRole.CONSUMER
    def __init__(self, name: str, address: str, authkey: str, topic: str):
        super().__init__(
                host=None, port=None, name=name,
                role=ProcessRole.CONSUMER, address=address,
                authkey=authkey
                )


        data = self.manager.lookup_process('test')
        self.consumer = Consumer(data[2], data[3], topic.encode())

    def run(self):
        # Read envelope with address
        [address, contents] = self.consumer.subscriber.recv_multipart()

        return unpack('f', contents)


m = MemoryManager(name='bokej', address=('127.0.0.1', 50000),
                  authkey=b'abracadabra', topic='test')

UPDATE_INTERVAL = 50
ROLLOVER = 200  # Number of displayed data points

source = ColumnDataSource({"x": [], "y": []})

m.run()

@count()
def update(x):
    y = m.run()
    source.stream({"x": [x], "y": [y]}, rollover=ROLLOVER)


p = figure(title='Supervet', plot_width=800, plot_height=800)
p.line("x", "y", source=source)
#, y_range=(-1.2, 1.2)
doc = curdoc()
doc.add_root(p)
doc.add_periodic_callback(update, UPDATE_INTERVAL)


