import time
import uuid
import logging

from enum import Enum
from typing import Optional
from collections import defaultdict
from multiprocessing.managers import BaseManager
from apscheduler.schedulers.background import BackgroundScheduler

logger = logging.getLogger(__name__)
logger.level = logging.INFO

TIMEOUT = 10


class Registry(object):
    def __init__(self):
        self._index = dict()
        self._name_index = defaultdict(list)

    def register(self, process):
        self._index[process.uuid] = (
            time.time(), process.host, process.port, process.ROLE
        )

        self._name_index[process.name].append(process.uuid)

    def lookup(self, name):
        return self._index.get(self._name_index.get(name))


class ProcessRole(Enum):
    CONSUMER = 1
    PRODUCER = 2


class ProcessManager(BaseManager):
    @classmethod
    def get_manager(cls, address, authkey):
        pm = cls(address=address, authkey=authkey)
        pm.register('_get_manager')
        pm.connect()
        return pm._get_manager()


class Manager(object):
    def __init__(self):
        self._index = dict()
        self._name_index = defaultdict(list)
        self._health = dict()
        self.scheduler = BackgroundScheduler()

        self.scheduler.add_job(
            self.check_health, 'interval', seconds=5, id='health-checker'
        )
        self.scheduler.start()

    def check_health(self):
        unhealthy = [k for k, v in self._health.items() if time.time() - v > TIMEOUT]
        for u in unhealthy:
            self._health.pop(u)
            x = self._index.pop(u)
            self._name_index.pop(x[5])
            logging.warning(f'Removed {x}')

    def register_process(self, uuid: str, name: str,
                         host: str, port: int, role: ProcessRole):

        self._index[uuid] = (
            time.time(), uuid, host, port, role, name
        )

        self._name_index[name].append(uuid)

        logger.warning(uuid + ' registered')

    def unregister_process(self, name: str, uuid: str):
        self._name_index.pop(name)
        self._index.pop(uuid)
        self._health.pop(uuid)
        logger.warning(uuid + ' unregistered')

    def lookup_process(self, name: str):
        return self._index.get(self._name_index.get(name)[0])

    def healthy(self, uuid: str):
        self._health[uuid] = time.time()

        logger.warning(uuid + ' is healthy')


class ManagedProcess(object):
    def __init__(self, host: Optional[str], port: Optional[int],
                 name: str, address: tuple, authkey: str, role: ProcessRole):

        self.uuid = str(uuid.uuid1())
        self.host = host
        self.name = name
        self.port = port
        self.role = role
        self.address = address
        self.authkey = authkey
        self.health_status = 0
        self.manager = None

        self.register()
        self.scheduler = BackgroundScheduler()
        self.scheduler.add_job(
            self.health_check,
            'interval', seconds=1, id='my_job_id'
            )

        self.scheduler.start()

        logger.info(f'instantiated {self.uuid} - {self.role} - {self.host}')

    def register(self):

        self.manager = ProcessManager.get_manager(
            address=self.address, authkey=self.authkey
            )

        self.manager.register_process(
            self.uuid, self.name,
            self.host, self.port, self.role
            )

    def health_check(self):
        try:
            self.manager.healthy(self.uuid)
        except Exception as Ex:
            try:
                self.register()
            except Exception as e:
                self.health_status = max(0, self.health_status - 1)
                logger.warning(f'{e}\nHealth level: {self.health_status}')
        else:
            self.health_status = min(5, self.health_status + 1)

    def __del__(self):
        self.manager.unregister_process(name=self.name, uuid=self.uuid)

if __name__ == '__main__':
    manager = Manager()
    ProcessManager.register('_get_manager', callable=lambda: manager)
    m = ProcessManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
    s = m.get_server()
    s.serve_forever()
